package main

import (
	"bufio"
	"flag"
	"fmt"
	"gitlab.com/Spouk/gotool/config"
	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v2"
	"log"
	"net"
	"os"
	"strings"
	"sync"
	"time"
)

func main() {

	var (
		user   string
		pass   string
		config string
		cli    bool //флаг показывающий,что использовать данные из параметров консоли а не из конфига для авторизации на удаленных машинах
	)
	flag.StringVar(&user, "user", "", "пользователь под которым производить добавление пользователя на удаленной машине")
	flag.StringVar(&pass, "pass", "", "пароль пользователя под которым производится добавление пользователя на удаленной машине")
	flag.StringVar(&config, "config", "", "файл конфигурации с полным путем")
	flag.BoolVar(&cli, "cli", false, "флаг, определяющий откуда брать данные пользователя под которым проходит добавление,консоль или конфиг")
	flag.Parse()

	//добавление с данными из консоли
	if cli && config != "" && user != "" && pass != "" {
		m := NewMakeUserHost(config)
		m.log.Println("==> running ext user,pass mode")

		sw := &sync.WaitGroup{}
		for _, x := range m.Cfg.Servers {
			sw.Add(1)
			go m.AddUserRemote(x.IP, x.Port, user, pass, sw)
		}
		sw.Wait()
		m.log.Println("завершение работы")

		//добавление с данными из конфига
	} else if cli == false && config != "" {
		m := NewMakeUserHost(config)
		m.log.Println("==> running config mode")

		sw := &sync.WaitGroup{}
		for _, x := range m.Cfg.Servers {
			sw.Add(1)
			go m.AddUserRemote(x.IP, x.Port, x.Sshuser, x.Sshpassword, sw)
		}
		sw.Wait()
		m.log.Println("завершение работы")

	} else {
		flag.PrintDefaults()
		os.Exit(1)
	}
}

type MakeUserHost struct {
	log *log.Logger
	Cfg *ConfigStruct
}

type ConfigStruct struct {
	Timeout     time.Duration `yaml:"timeout"`
	Useradd     string        `yaml:"useradd"`
	Passwordadd string        `yaml:"passwordadd"`
	Comadduser  string        `yaml:"comadduser"`
	Servers     []*server     `yaml:"servers"`
}
type server struct {
	IP          string `yaml:"ip"`
	Port        string `yaml:"sshport"`
	Sshuser     string `yaml:"sshuser"`
	Sshpassword string `yaml:"sshpassword"`
}

func NewMakeUserHost(configFname string) *MakeUserHost {
	c := &MakeUserHost{
		log: log.New(os.Stdout, "[work-adduser] ", log.LstdFlags),
	}
	cc := config.NewConf("/tmp/", os.Stdout)
	tmpCfg := &ConfigStruct{}
	err := cc.ReadConfig(configFname, tmpCfg)
	if err != nil {
		c.log.Fatal(err)
	}
	c.Cfg = tmpCfg
	c.log.Printf("Servers: %v\n", c.Cfg.Servers[0])

	return c
}

func (m *MakeUserHost) exec(c *ssh.Client, command string) string {
	cc, err := c.NewSession()
	if err != nil {
		m.log.Printf("Ошибка выполнение команды: %v\n", err)
	}

	res, err := cc.CombinedOutput(command)
	if err != nil {
		log.Printf("Ошибка выполнения команды: %v : %s\n", err, string(res))
	}
	return string(res)
}
func (m *MakeUserHost) connectssh(host, port, user, pass string) (*ssh.Client, error) {
	m.log.Printf("[%v] %s %s %s\n", host, port, user, pass)
	config := &ssh.ClientConfig{
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		User:            user,
		Auth: []ssh.AuthMethod{
			ssh.Password(pass),
		},
		Timeout: time.Second * m.Cfg.Timeout,
	}
	//connect
	connection, err := ssh.Dial("tcp", net.JoinHostPort(host, port), config)
	if err != nil {
		//m.log.Printf("Ошибка подключения: [%s] `%s`  ", host, err)
		return nil, err
	}
	return connection, nil
}
func (m *MakeUserHost) AddUserRemote(host, port, user, pass string, sw *sync.WaitGroup) {
	defer sw.Done()
	com := fmt.Sprintf(m.Cfg.Comadduser, m.Cfg.Passwordadd, m.Cfg.Useradd)
	con, err := m.connectssh(host, port, user, pass)
	if err != nil {
		m.log.Printf("Ошибка создания подключения: %s  %v\n", host, err)
		return
	}
	res := m.exec(con, com)
	m.log.Printf("[ OK-exec :: %s] =>:  %v   \n", host, res)
	return
}
func parselist() {
	list := "/home/spouk/stock/s.develop/go/src/gitlab.com/work-adduser/listservers"
	f, _ := os.OpenFile(list, os.O_RDWR, 0666)
	resList := []*server{}
	scan := bufio.NewScanner(f)

	for scan.Scan() {
		ip := strings.Split(scan.Text(), ":")[0]
		nc := &server{}
		nc.IP = ip
		nc.Sshpassword = ""
		nc.Port = "22"
		nc.Sshuser = ""
		resList = append(resList, nc)
	}
	enc := yaml.NewEncoder(os.Stdout)
	enc.Encode(resList)
	//fmt.Printf("Reslist: %v\n", resList)
	os.Exit(1)

}
